package com.canteenordering.dao;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;
import org.springframework.util.StringUtils;

import com.canteenordering.javabean.notice;


public interface noticeMapper {

	@Insert("insert into notice(noticeCode,noticeName,noticeContent,addTime) values(#{noticeCode},#{noticeName},#{noticeContent},NOW())")
	public int insertnoticeInfo(notice notice);

	@SelectProvider(type = Info.class, method = "getnoticeListInfo")
	public List<notice> getnoticeListInfo();

	@Select("select * from notice  WHERE noticeCode =#{noticeCode} ")
	public notice getnoticeInfo(@Param("noticeCode") String noticeCode);
	
	@Delete("DELETE FROM notice WHERE noticeCode =#{noticeCode}")
	public int deletenotice(@Param("noticeCode") String noticeCode);

	class Info {
		public String getnoticeListInfo(String gradeName) {
			String sql = "select * from notice  WHERE 1=1 ORDER BY addTime DESC ";
			return sql;
		}
	}
	
}
