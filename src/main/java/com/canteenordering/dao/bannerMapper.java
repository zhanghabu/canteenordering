package com.canteenordering.dao;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.Update;
import org.springframework.util.StringUtils;

import com.canteenordering.javabean.banner;
import com.canteenordering.javabean.business;

public interface bannerMapper {

	/**
	 * banner新增
	 * 
	 * @param banner
	 * @return
	 */
	@Insert("INSERT INTO banner (bannerCode,bannerType,bannerName,enable,activity,addTime,num,bannerPic,bannerInfo,businessCode,"
			+ "commodityCode) VALUES (#{bannerCode},#{bannerType},#{bannerName},1,1,NOW(),#{num},#{bannerPic},#{bannerInfo},#{businessCode},#{commodityCode})")
	public int insertbannerInfo(banner banner);
	
	
	/**
	 * banner列表
	 * @param bannerType
	 * @param enable
	 * @return
	 */
	@SelectProvider(type = Info.class, method = "getbannerListInfo")
	public List<banner> getbannerListInfo(String bannerType,String enable);
	
	/**
	 * banner删除
	 * @param bannerCode
	 * @return
	 */
	@Update("UPDATE banner SET activity=0 WHERE bannerCode=#{bannerCode} ")
	public int banneractivity(@Param("bannerCode") String bannerCode);
	
	/**
	 * banner禁用启用
	 * @param enable
	 * @param bannerCode
	 * @return
	 */
	@Update("UPDATE banner SET enable=#{enable} WHERE bannerCode=#{bannerCode} ")
	public int bannerenable(@Param("enable") int enable,@Param("bannerCode") String bannerCode);
	
	
	
	/**
	 * 
	 * 列表查询
	 *
	 */
	class Info {
		public String getbannerListInfo(String bannerType,String enable) {
			String sql = "select * from banner  WHERE activity=1  ";
			// 当值不为空时当做条件参数查询
			if (!StringUtils.isEmpty(bannerType)) {
				sql += " AND bannerType = '" + bannerType + "'";
			}
			if (!StringUtils.isEmpty(enable)) {
				sql += " AND enable = " + enable + "";
			}
			sql += " ORDER BY addTime DESC ";
			return sql;
		}
	}

}
