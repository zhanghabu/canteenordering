package com.canteenordering.dao;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.Update;
import org.springframework.util.StringUtils;

import com.canteenordering.javabean.business;
import com.canteenordering.javabean.customer;

public interface customerMapper {

	/**
	 * 用户注册
	 * @param customer
	 * @return
	 */
	@Insert("INSERT INTO customer (customerCode,customerName,name,"
			+ "phone,enable,addTime,activity,openid,customerPhoto,"
			+ "password,address) VALUES (#{customerCode},#{customerName},"
			+ "#{name},#{phone},1,NOW(),1,#{openid},#{customerPhoto},'123456',#{address})")
	public int insertcustomerInfo(customer customer);
	
	/**
	 * 查询数据库有没有重复账号
	 * @param businessName
	 * @return
	 */
	@Select("select COUNT(*) from customer  WHERE customerName =#{customerName} ")
	public Integer getcustomerName(@Param("customerName") String customerName);

	/**
	 * 查询当前openid是否绑定别的账号
	 * @param openid
	 * @return
	 */
	@Select("select COUNT(*) from customer  WHERE openid =#{openid} AND activity=1 ")
	public Integer getopenid(@Param("openid") String openid);

	
	
	/**
	 * 用户列表查询
	 * @param name
	 * @param phone
	 * @param enable
	 * @return
	 */
	@SelectProvider(type = Info.class, method = "getcustomerListInfo")
	public List<customer> getcustomerListInfo(String name, String phone,String enable);

	/**
	 * 用户详情
	 * @param customerCode
	 * @return
	 */
	@Select("select * from customer  WHERE customerCode =#{customerCode} ")
	public customer getcustomerCodeInfo(@Param("customerCode") String customerCode);

	
	/**
	 * 用户通过openid登录
	 * 
	 * @param openid
	 * @return
	 */
	@Select("select * from customer  WHERE openid =#{openid} AND activity=1 AND enable=1")
	public customer openidlogincustomer(@Param("openid") String openid);
	

	/**
	 * 绑定openid
	 * @param openid
	 * @param customerName
	 * @param password
	 * @return
	 */
	@Update("UPDATE customer SET openid=#{openid} WHERE customerName=#{customerName} AND password=#{password} ")
	public int bindingcustomer(@Param("openid") String openid, @Param("customerName") String customerName,
			@Param("password") String password);

	
	/**
	 * 修改用户信息
	 * @param customer
	 * @return
	 */
	@Update("UPDATE customer SET name=#{name},customerPhoto=#{customerPhoto},address=#{address} WHERE customerCode=#{customerCode}")
	public int upcustomerInfo(customer customer);

	/**
	 * 修改密码
	 * @param newpassword
	 * @param customerName
	 * @param oldpassword
	 * @return
	 */
	@Update("UPDATE customer SET password=#{newpassword} WHERE customerName=#{customerName} AND password=#{oldpassword} ")
	public int upcustomerpassword(@Param("newpassword") String newpassword, @Param("customerName") String customerName,
			@Param("oldpassword") String oldpassword);

	
	
	/**
	 * 
	 * 列表查询
	 *
	 */
	class Info {
		public String getcustomerListInfo(String name, String phone,String enable) {
			String sql = "select * from customer  WHERE activity=1  ";
			// 当值不为空时当做条件参数查询
			if (!StringUtils.isEmpty(name)) {
				sql += " AND name LIKE '%" + name + "%'";
			}
			if (!StringUtils.isEmpty(phone)) {
				sql += " AND phone = '" + phone + "'";
			}
			if (!StringUtils.isEmpty(enable)) {
				sql += " AND enable = " + enable + "";
			}
			sql += " ORDER BY addTime DESC ";
			return sql;
		}
	}

}
