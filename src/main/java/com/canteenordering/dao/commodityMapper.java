package com.canteenordering.dao;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.Update;
import org.springframework.util.StringUtils;

import com.canteenordering.javabean.business;
import com.canteenordering.javabean.commodity;

public interface commodityMapper {

	/**
	 * 商家添加商品
	 * 
	 * @param business
	 * @return
	 */
	@Insert("INSERT INTO commodity (commodityCode,commodityName," + "commodityContent,commodityStock,salesVolume,"
			+ "menuCode,businessCode,commodityphoto,enable," + "addTime,activity,price)"
			+ " VALUES (#{commodityCode},#{commodityName}," + "#{commodityContent},#{commodityStock},"
			+ "0,#{menuCode},#{businessCode}," + "#{commodityphoto},1,NOW(),1,#{price})")
	public int insertcommodity(commodity commodity);

	/**
	 * 查询该菜单下是否有重复的菜名
	 * 
	 * @param menuCode
	 * @param commodityName
	 * @return
	 */
	@Select("select commodityName from commodity  WHERE menuCode =#{menuCode} AND commodityName =#{commodityName}")
	public String getcommodityName(@Param("menuCode") String menuCode, @Param("commodityName") String commodityName);

	/**
	 * 查询该商品销量
	 * 
	 * @param commodityCode
	 * @return
	 */
	@Select("SELECT num FROM order_commodity WHERE commodityCode=#{commodityCode}")
	public List<Integer> getcommoditynum(@Param("commodityCode") String commodityCode);

	/**
	 * 查询商品列表
	 * 
	 * @param commodityName
	 * @param commodityContent
	 * @param menuCode
	 * @param enable
	 * @param businessCode
	 * @return
	 */
	@SelectProvider(type = Info.class, method = "getcommodityListInfo")
	public List<commodity> getcommodityListInfo(String commodityName, String commodityContent, String menuCode,
			String enable, String businessCode);

	/**
	 * 商品详情查询
	 * 
	 * @param commodityCode
	 * @return
	 */
	@Select("select * from commodity  WHERE commodityCode =#{commodityCode} ")
	public commodity getcommodityInfo(@Param("commodityCode") String commodityCode);

	/**
	 * 查询商家名称
	 * 
	 * @param businessCode
	 * @return
	 */
	@Select("select name from business  WHERE businessCode =#{businessCode} ")
	public String getbusinessCodeInfo(@Param("businessCode") String businessCode);

	/**
	 * 查询菜单名称
	 * 
	 * @param menuCode
	 * @return
	 */
	@Select("select menuName from businessmenu  WHERE menuCode =#{menuCode} ")
	public String getbusinessmenuInfo(@Param("menuCode") String menuCode);

	/**
	 * 商品禁用启用
	 * 
	 * @param enable
	 * @param commodityCode
	 * @return
	 */
	@Update("UPDATE commodity SET enable=#{enable} WHERE commodityCode=#{commodityCode} ")
	public int updatecommodityenable(@Param("enable") int enable, @Param("commodityCode") String commodityCode);

	/**
	 * 商品删除接口
	 * 
	 * @param commodityCode
	 * @return
	 */
	@Update("UPDATE commodity SET activity=0 WHERE commodityCode=#{commodityCode} ")
	public int commodityactivity(@Param("commodityCode") String commodityCode);

	/**
	 * 商品修改
	 * 
	 * @param commodity
	 * @return
	 */
	@Update("UPDATE commodity SET commodityName=#{commodityName}," + "commodityContent=#{commodityContent},"
			+ "commodityStock=#{commodityStock}," + "menuCode=#{menuCode}," + "commodityphoto=#{commodityphoto},"
			+ "price=#{price},updateTime=NOW() WHERE commodityCode=#{commodityCode} ")
	public int upcommodityInfo(commodity commodity);

	/**
	 * 
	 * 列表查询
	 *
	 */
	class Info {
		public String getcommodityListInfo(String commodityName, String commodityContent, String menuCode,
				String enable, String businessCode) {
			String sql = "select * from commodity  WHERE activity=1  ";
			// 当值不为空时当做条件参数查询
			if (!StringUtils.isEmpty(commodityName)) {
				sql += " AND commodityName LIKE '%" + commodityName + "%'";
			}
			if (!StringUtils.isEmpty(commodityContent)) {
				sql += " AND commodityContent LIKE '%" + commodityContent + "%'";
			}
			if (!StringUtils.isEmpty(menuCode)) {
				sql += " AND menuCode = '" + menuCode + "'";
			}
			if (!StringUtils.isEmpty(businessCode)) {
				sql += " AND businessCode = '" + businessCode + "'";
			}
			if (!StringUtils.isEmpty(enable)) {
				sql += " AND enable = " + enable + "";
			}
			sql += " ORDER BY addTime DESC ";
			return sql;
		}
	}

}
