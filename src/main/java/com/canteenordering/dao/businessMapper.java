package com.canteenordering.dao;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.Update;
import org.springframework.util.StringUtils;

import com.canteenordering.javabean.business;

public interface businessMapper {

	/**
	 * 商家注册
	 * 
	 * @param business
	 * @return
	 */
	@Insert("INSERT INTO business (businessCode,businessName,name,phone,"
			+ "address,isAdmin,enable,addTime,activity,state,openid,"
			+ "businessPhoto,password,notice,isRecommend) VALUES (#{businessCode},#{businessName},"
			+ "#{name},#{phone},#{address},0,1,NOW(),1,0,#{openid},#{businessPhoto},'123456',#{notice},0)")
	public int insertbusinessInfo(business business);
	
	/**
	 * 查询数据库有没有重复账号
	 * @param businessName
	 * @return
	 */
	@Select("select COUNT(*) from business  WHERE businessName =#{businessName} AND activity=1 ")
	public Integer getbusinessName(@Param("businessName") String businessName);

	/**
	 * 查询当前openid是否绑定别的账号
	 * @param openid
	 * @return
	 */
	@Select("select COUNT(*) from business  WHERE openid =#{openid} AND activity=1 ")
	public Integer getopenid(@Param("openid") String openid);

	

	/**
	 * 商家列表查询
	 * 
	 * @param name
	 * @param phone
	 * @param address
	 * @param enable
	 * @param state
	 * @return
	 */
	@SelectProvider(type = Info.class, method = "getbusinessListInfo")
	public List<business> getbusinessListInfo(String name, String phone, String address, String enable, String state,String isRecommend);

	/**
	 * 商家详情查询
	 * 
	 * @param businessCode
	 * @return
	 */
	@Select("select * from business  WHERE businessCode =#{businessCode} ")
	public business getbusinessCodeInfo(@Param("businessCode") String businessCode);

	/**
	 * 商家pc登录
	 * 
	 * @param businessName
	 * @param password
	 * @return
	 */
	@Select("select * from business  WHERE businessName =#{businessName} AND password =#{password} AND activity=1 AND enable=1")
	public business loginbusiness(@Param("businessName") String businessName, @Param("password") String password);

	/**
	 * 商家通过openid登录
	 * 
	 * @param openid
	 * @return
	 */
	@Select("select * from business  WHERE openid =#{openid} AND activity=1 AND enable=1")
	public business openidloginbusiness(@Param("openid") String openid);

	/**
	 * 修改商家营业状态 0 打样 1营业
	 * 
	 * @param state
	 * @param businessCode
	 * @return
	 */
	@Update("UPDATE business SET state=#{state} WHERE businessCode=#{businessCode} ")
	public int updatebusinessstate(@Param("state") int state, @Param("businessCode") String businessCode);

	/**
	 * 商家禁用启用
	 * 
	 * @param enable
	 * @param businessCode
	 * @return
	 */
	@Update("UPDATE business SET enable=#{enable} WHERE businessCode=#{businessCode} ")
	public int updatebusinessenable(@Param("enable") int enable, @Param("businessCode") String businessCode);

	/**
	 * 设置商家是否推荐
	 * @param isRecommend
	 * @param businessCode
	 * @return
	 */
	@Update("UPDATE business SET isRecommend=#{isRecommend} WHERE businessCode=#{businessCode} ")
	public int updatebusinessisRecommend(@Param("isRecommend") int isRecommend, @Param("businessCode") String businessCode);

	
	
	/**
	 * 绑定openid
	 * @param openid
	 * @param businessName
	 * @param password
	 * @return
	 */
	@Update("UPDATE business SET openid=#{openid} WHERE businessName=#{businessName} AND password=#{password} ")
	public int bindingbusiness(@Param("openid") String openid, @Param("businessName") String businessName,
			@Param("password") String password);

	/**
	 * 商家删除接口
	 * 
	 * @param businessCode
	 * @return
	 */
	@Update("UPDATE business SET activity=0 WHERE businessCode=#{businessCode} ")
	public int businessactivity(@Param("businessCode") String businessCode);

	/**
	 * 修改商家信息
	 * @param business
	 * @return
	 */
	@Update("UPDATE business SET name=#{name},address=#{address},businessPhoto=#{businessPhoto},notice=#{notice} WHERE businessCode=#{businessCode}")
	public int upbusinessInfo(business business);

	/**
	 * 修改密码
	 * @param newpassword
	 * @param businessName
	 * @param oldpassword
	 * @return
	 */
	@Update("UPDATE business SET password=#{newpassword} WHERE businessName=#{businessName} AND password=#{oldpassword} ")
	public int upbusinesspassword(@Param("newpassword") String newpassword, @Param("businessName") String businessName,
			@Param("oldpassword") String oldpassword);

	
	/**
	 * 
	 * 列表查询
	 *
	 */
	class Info {
		public String getbusinessListInfo(String name, String phone, String address, String enable, String state,String isRecommend) {
			String sql = "select * from business  WHERE activity=1 AND isAdmin=0  ";
			// 当值不为空时当做条件参数查询
			if (!StringUtils.isEmpty(name)) {
				sql += " AND name LIKE '%" + name + "%'";
			}
			if (!StringUtils.isEmpty(address)) {
				sql += " AND address LIKE '%" + address + "%'";
			}
			if (!StringUtils.isEmpty(phone)) {
				sql += " AND phone = '" + phone + "'";
			}
			if (!StringUtils.isEmpty(enable)) {
				sql += " AND enable = " + enable + "";
			}
			if (!StringUtils.isEmpty(state)) {
				sql += " AND state = " + state + "";
			}
			if (!StringUtils.isEmpty(isRecommend)) {
				sql += " AND isRecommend = " + isRecommend + "";
			}
			sql += " ORDER BY addTime DESC ";
			return sql;
		}
	}

}
