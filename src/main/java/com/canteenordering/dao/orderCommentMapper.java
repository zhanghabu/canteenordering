package com.canteenordering.dao;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.Update;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;

import com.canteenordering.dao.commodityMapper.Info;
import com.canteenordering.javabean.business;
import com.canteenordering.javabean.commodity;
import com.canteenordering.javabean.order;
import com.canteenordering.javabean.orderComment;
import com.canteenordering.javabean.orderCommodity;

public interface orderCommentMapper {

	/**
	 * 新增评论
	 * 
	 * @param orderComment
	 * @return
	 */
	@Insert("INSERT INTO order_comment (commentCode,content," + "customerCode,customerName,cname,customerPhoto,"
			+ "businessCode,businessName,commentNum,enable,"
			+ "addTime,activity,bname,businessPhoto,orderCode,isShow,commentPhoto)"
			+ " VALUES (#{info.commentCode},#{info.content},#{info.customerCode},"
			+ "#{info.customerName},#{info.cname},#{info.customerPhoto},"
			+ "#{info.businessCode},#{info.businessName},#{info.commentNum},1,NOW(),1,"
			+ "#{info.bname},#{info.businessPhoto},#{info.orderCode},0,#{info.commentPhoto})")
	public int insertorderCommentInfo(@Param("info") orderComment orderComment);

	/**
	 * 修改是否已评论
	 * @param orderCode
	 * @return
	 */
	@Update("UPDATE `order` SET isComment=1,updateTime=NOW() WHERE orderCode=#{orderCode} ")
	public int updateorderisComment(@Param("orderCode") String orderCode);

	/**
	 * 评论列表
	 * 
	 * @param customerCode
	 * @param businessCode
	 * @param isShow
	 * @param orderCode
	 * @return
	 */
	@SelectProvider(type = Info.class, method = "getordercommentListInfo")
	public List<orderComment> getordercommentListInfo(String customerCode, String businessCode, String isShow,
			String orderCode);

	/**
	 * 评论详情
	 * 
	 * @param commentCode
	 * @return
	 */
	@Select("select * from order_comment  WHERE activity=1 AND enable=1 AND commentCode=#{commentCode}")
	public orderComment getorderCommentInfo(@Param("commentCode") String commentCode);

	/**
	 * 通过订单编号查询评论
	 * @param orderCode
	 * @return
	 */
	@Select("select * from order_comment  WHERE activity=1 AND enable=1 AND orderCode=#{orderCode}")
	public orderComment getorderCodeInfo(@Param("orderCode") String orderCode);

	
	
	/**
	 * 是否显示评论
	 * 
	 * @param isShow
	 * @param commentCode
	 * @return
	 */
	@Update("UPDATE order_comment SET isShow=#{isShow},updateTime=NOW() WHERE commentCode=#{commentCode} ")
	public int updateorderComment(@Param("isShow") int isShow, @Param("commentCode") String commentCode);

	/**
	 * 
	 * 列表查询
	 *
	 */
	class Info {
		public String getordercommentListInfo(String customerCode, String businessCode, String isShow,
				String orderCode) {
			String sql = "select * from order_comment  WHERE activity=1 AND enable=1 ";
			// 当值不为空时当做条件参数查询
			if (!StringUtils.isEmpty(customerCode)) {
				sql += " AND customerCode = '" + customerCode + "'";
			}
			if (!StringUtils.isEmpty(businessCode)) {
				sql += " AND businessCode = '" + businessCode + "'";
			}
			if (!StringUtils.isEmpty(orderCode)) {
				sql += " AND orderCode = '" + orderCode + "'";
			}
			if (!StringUtils.isEmpty(isShow)) {
				sql += " AND isShow = " + isShow + "";
			}

			sql += " ORDER BY addTime DESC ";
			return sql;
		}
	}

}
