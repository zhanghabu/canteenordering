package com.canteenordering.dao;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.Update;
import org.springframework.util.StringUtils;

import com.canteenordering.javabean.business;
import com.canteenordering.javabean.businessmenu;

public interface businessmenuMapper {

	/**
	 * 商家菜单新增
	 * @param businessmenu
	 * @return
	 */
	@Insert("INSERT INTO businessmenu (menuCode,menuName,enable,"
			+ "addTime,activity,businessCode,num)"
			+ " VALUES (#{menuCode},#{menuName},1,NOW(),1,#{businessCode},#{num}) ")
	public int insertbusinessmenu(businessmenu businessmenu);
	
	
	/**
	 * 查询商家菜单
	 * @param businessCode
	 * @return
	 */
	@Select("select * from businessmenu  WHERE businessCode =#{businessCode} AND activity=1 AND enable=#{enable}")
	public List<businessmenu> gettbusinessmenuList(@Param("businessCode") String businessCode,@Param("enable") int enable);

	/**
	 * 查询是否有重复的名称
	 * @param businessCode
	 * @param menuName
	 * @return
	 */
	@Select("select menuName from businessmenu  WHERE businessCode =#{businessCode} AND activity=1 AND enable=1 AND menuName =#{menuName}")
	public String gettmenuName(@Param("businessCode") String businessCode,@Param("menuName") String menuName);

	
	
	/**
	 * 商家菜单禁用启用
	 * @param enable
	 * @param businessCode
	 * @return
	 */
	@Update("UPDATE businessmenu SET enable=#{enable} WHERE menuCode=#{menuCode} ")
	public int updatebusinessmenuenable(@Param("enable") int enable, @Param("menuCode") String menuCode);

	
	
	/**
	 * 商家菜单删除接口
	 * @param menuCode
	 * @return
	 */
	@Update("UPDATE businessmenu SET activity=0 WHERE menuCode=#{menuCode} ")
	public int businessmenuactivity(@Param("menuCode") String menuCode);

	/**
	 * 修改菜单名称
	 * @param menuName
	 * @param menuCode
	 * @return
	 */
	@Update("UPDATE businessmenu SET menuName=#{menuName} WHERE menuCode=#{menuCode} ")
	public int upbusinessmenuName(@Param("menuName") String menuName, @Param("menuCode") String menuCode);

	/**
	 * 修改顺序
	 * @param num
	 * @param menuCode
	 * @return
	 */
	@Update("UPDATE businessmenu SET num=#{num} WHERE menuCode=#{menuCode} ")
	public int upbusinessmenunum(@Param("num") int num, @Param("menuCode") String menuCode);

	
}
