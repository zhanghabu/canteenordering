package com.canteenordering.dao;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.Update;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;

import com.canteenordering.dao.commodityMapper.Info;
import com.canteenordering.javabean.business;
import com.canteenordering.javabean.commodity;
import com.canteenordering.javabean.order;
import com.canteenordering.javabean.orderCommodity;
import com.canteenordering.javabean.orderVO1;

public interface orderMapper {

	/**
	 * 生产主订单
	 * 
	 * @param order
	 * @return
	 */
	@Insert("INSERT INTO `order` (orderCode,state,businessCode," + "bname,bphone,sumPrice,enable,addTime,activity,"
			+ "remarks,customerCode,cname,cphone,reminderNum,businessPhoto,customerPhoto,isComment,month,day,year)"
			+ " VALUES (#{order.orderCode},0,#{order.businessCode}"
			+ ",#{order.bname},#{order.bphone},#{order.sumPrice},1,NOW(),1,#{order.remarks}"
			+ ",#{order.customerCode},#{order.cname},#{order.cphone},0,#{order.businessPhoto},#{order.customerPhoto},0,#{month},#{day},#{year})")
	public int insertorderInfo(@Param("order") order order, @Param("month") int month, @Param("day") int day,
			@Param("year") String year);

	/**
	 * 生产订单内商品
	 * 
	 * @param orderCommodity
	 * @return
	 */
	@Insert("INSERT INTO order_commodity (orderCode,commodityCode," + "commodityName,num,commodityphoto,price,enable,"
			+ "addTime,activity) VALUES (#{orderCode}," + "#{commodityCode},#{commodityName},#{num},"
			+ "#{commodityphoto},#{price},1,NOW(),1)")
	public int insertordercommodityInfo(orderCommodity orderCommodity);

	/**
	 * 订单列表查询
	 * 
	 * @param state
	 * @param businessCode
	 * @param bphone
	 * @param bname
	 * @param customerCode
	 * @param cname
	 * @param cphone
	 * @return
	 */
	@SelectProvider(type = Info.class, method = "getorderListInfo")
	public List<order> getorderListInfo(String state, String businessCode, String bphone, String bname,
			String customerCode, String cname, String cphone,String stateTime,String endTime);

	/**
	 * 订单详情
	 * 
	 * @param orderCode
	 * @return
	 */
	@Select("select * from `order`  WHERE orderCode =#{orderCode} ")
	public order getorderInfo(@Param("orderCode") String orderCode);

	/**
	 * 统计图
	 * 
	 * @param year
	 * @param month
	 * @param type
	 * @return
	 */
	@SelectProvider(type = Info.class, method = "getordertongji")
	public List<orderVO1> getordertongji(@Param("year") String year, @Param("month") String month,
			@Param("type") int type,@Param("businessCode") String businessCode);

	/**
	 * 订单下的商品
	 * 
	 * @param orderCode
	 * @return
	 */
	@Select("select * from order_commodity  WHERE orderCode =#{orderCode} ")
	public List<orderCommodity> getorderCommodityInfo(@Param("orderCode") String orderCode);

	/**
	 * 修改订单状态
	 * 
	 * @param state
	 * @param orderCode
	 * @return
	 */
	@Update("UPDATE `order` SET state=#{state},updateTime=NOW() WHERE orderCode=#{orderCode} ")
	public int updateorderstate(@Param("state") int state, @Param("orderCode") String orderCode);

	/**
	 * 催单
	 * 
	 * @param orderCode
	 * @return
	 */
	@Update("UPDATE `order` SET reminderNum=reminderNum+1,updateTime=NOW() WHERE orderCode=#{orderCode} ")
	public int updateorderreminderNum(@Param("orderCode") String orderCode);

	/**
	 * 
	 * 列表查询
	 *
	 */
	class Info {
		public String getorderListInfo(String state, String businessCode, String bphone, String bname,
				String customerCode, String cname, String cphone,
				String stateTime,String endTime) {
			String sql = "select * from `order`  WHERE activity=1 AND enable=1 ";
			// 当值不为空时当做条件参数查询
			if (!StringUtils.isEmpty(state)) {
				sql += " AND state = " + state + "";
			}
			if (!StringUtils.isEmpty(businessCode)) {
				sql += " AND businessCode = '" + businessCode + "'";
			}
			if (!StringUtils.isEmpty(bphone)) {
				sql += " AND bphone = '" + bphone + "'";
			}
			if (!StringUtils.isEmpty(bname)) {
				sql += " AND bname LIKE '%" + bname + "%'";
			}

			if (!StringUtils.isEmpty(customerCode)) {
				sql += " AND customerCode = '" + customerCode + "'";
			}
			if (!StringUtils.isEmpty(cphone)) {
				sql += " AND cphone = '" + cphone + "'";
			}
			if (!StringUtils.isEmpty(cname)) {
				sql += " AND cname LIKE '%" + cname + "%'";
			}
			
			if (!StringUtils.isEmpty(stateTime)) {
				sql += " AND addTime >= '" + stateTime + " 00:00:00'";
			}
			
			if (!StringUtils.isEmpty(stateTime)) {
				sql += " AND addTime <= '" + endTime + " 23:59:59'";
			}

			sql += " ORDER BY addTime DESC ";
			return sql;
		}

		public String getordertongji(String year, String month, int type, String businessCode) {
			String sql = "select SUM(sumPrice) AS priceSum,year,month,day,COUNT(*) AS orderNum  from `order`  WHERE activity=1 AND enable=1 ";
			// 当值不为空时当做条件参数查询
			if (!StringUtils.isEmpty(businessCode)) {
				sql += " AND businessCode = '" + businessCode + "'";
			}
			if (!StringUtils.isEmpty(year)) {
				sql += " AND year = '" + year + "'";
			}
			if (!StringUtils.isEmpty(month)) {
				sql += " AND month = " + month + "";
			}
			if (type == 1) {
				sql += " GROUP BY month";
			} else {
				sql += " GROUP BY day";
			}

			return sql;
		}

	}

}
