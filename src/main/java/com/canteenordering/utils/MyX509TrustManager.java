package com.canteenordering.utils;

import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.X509TrustManager;

/**
 *Copyright © 2017希霆云梦. All rights reserved.
 *
 *@Prject HYTC
 *@Title MessageUtil.java
 *@date 2017年4月11日 下午3:20:49
 *@author ZhuPengYan
 *@version: v1.0
 *@description  证书信任管理器（用于https请求）
 */ 
public class MyX509TrustManager implements X509TrustManager {

	public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
	}

	public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
	}

	public X509Certificate[] getAcceptedIssuers() {
		return null;
	}
}
