package com.canteenordering.utils;

import java.util.Random;

import com.alibaba.fastjson.JSONObject;

public class PublicUtils {
	
	
	/**
	 * 统一返回参数
	 * @param body
	 * @param state
	 * @return
	 */
	public static JSONObject getReturn(String body,String state){
		
		JSONObject rjson = new JSONObject();
		rjson.put("flag", state);
		rjson.put("msg", body);
		return rjson;
	}
	
	/**
	 * 备用方法：随机生成字符串
	 * 
	 * @param length	生成字符串的长度  
	 * @return
	 */
	public static String getRandomString(int length) { 
		String base = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";   

		Random random = new Random();     

		StringBuffer sb = new StringBuffer();     

		for (int i = 0; i < length; i++) {     
			int number = random.nextInt(base.length());     
			sb.append(base.charAt(number));     
		}     
		return sb.toString();     
	} 
}
