package com.canteenordering.controller;

import java.io.UnsupportedEncodingException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.canteenordering.javabean.business;
import com.canteenordering.javabean.customer;
import com.canteenordering.javabean.notice;
import com.canteenordering.service.businessService;
import com.canteenordering.service.customerService;
import com.canteenordering.service.noticeService;
import com.github.pagehelper.PageInfo;

@CrossOrigin(origins = { "*", "null" })
@ResponseBody
@RestController
public class customerController {

	@Autowired
	private customerService customerinfo;

	/**
	 * 用户注册
	 * @param customer
	 * @return
	 */
	@RequestMapping(value = "/insertcustomerInfo", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public JSONObject insertcustomerInfo(@RequestBody customer customer) throws UnsupportedEncodingException {
		JSONObject rbody = customerinfo.insertcustomerInfo(customer);
		return rbody;
	}

	/**
	 * 用户通过openid登录
	 * @param openid
	 * @return
	 */
	@RequestMapping(value = "/openidlogincustomer", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public customer openidlogincustomer(String openid) throws UnsupportedEncodingException {
		customer rbody = customerinfo.openidlogincustomer(openid);
		return rbody;
	}

	/**
	 * 绑定openid
	 * @param openid
	 * @param customerName
	 * @param password
	 * @return
	 */
	@RequestMapping(value = "/bindingcustomer", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public JSONObject bindingcustomer(String openid, String customerName, String password)
			throws UnsupportedEncodingException {
		JSONObject rbody = customerinfo.bindingcustomer(openid, customerName, password);
		return rbody;
	}


	/**
	 * 修改用户信息
	 * @param customer
	 * @return
	 */
	@RequestMapping(value = "/upcustomerInfo", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public JSONObject upbusinessInfo(@RequestBody customer customer) throws UnsupportedEncodingException {
		JSONObject rbody = customerinfo.upcustomerInfo(customer);
		return rbody;
	}

	/**
	 * 修改密码
	 * @param newpassword
	 * @param customerName
	 * @param oldpassword
	 * @return
	 */
	@RequestMapping(value = "/upcustomerpassword", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public JSONObject upcustomerpassword(String newpassword, String customerName, String oldpassword)
			throws UnsupportedEncodingException {
		JSONObject rbody = customerinfo.upcustomerpassword(newpassword, customerName, oldpassword);
		return rbody;
	}

}
