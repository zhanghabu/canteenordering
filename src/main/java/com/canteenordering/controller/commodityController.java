package com.canteenordering.controller;

import java.io.UnsupportedEncodingException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.canteenordering.javabean.business;
import com.canteenordering.javabean.commodity;
import com.canteenordering.javabean.notice;
import com.canteenordering.service.businessService;
import com.canteenordering.service.commodityService;
import com.canteenordering.service.noticeService;
import com.github.pagehelper.PageInfo;

@CrossOrigin(origins = { "*", "null" })
@ResponseBody
@RestController
public class commodityController {

	@Autowired
	private commodityService commodityinfo;

	/**
	 * 商品新增
	 * 
	 * @param commodity
	 * @return
	 */
	@RequestMapping(value = "/insertcommodity", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public JSONObject insertcommodity(@RequestBody commodity commodity) throws UnsupportedEncodingException {
		JSONObject rbody = commodityinfo.insertcommodity(commodity);
		return rbody;
	}

	/**
	 * 查询商品列表
	 * @param commodityName
	 * @param commodityContent
	 * @param menuCode
	 * @param enable
	 * @param businessCode
	 * @param page
	 * @param pageNum
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	@RequestMapping(value = "/getcommodityListInfo", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public PageInfo<commodity> getcommodityListInfo(String commodityName, String commodityContent, String menuCode,
			String enable, String businessCode, int page, int pageNum) throws UnsupportedEncodingException {
		PageInfo<commodity> rbody = commodityinfo.getcommodityListInfo(commodityName, commodityContent, menuCode,
				enable, businessCode, page, pageNum);
		return rbody;
	}

	/**
	 * 商品详情查询
	 * 
	 * @param commodityCode
	 * @return
	 */
	@RequestMapping(value = "/getcommodityInfo", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public commodity getcommodityInfo(String commodityCode) throws UnsupportedEncodingException {
		commodity rbody = commodityinfo.getcommodityInfo(commodityCode);
		return rbody;
	}

	/**
	 * 商品禁用启用
	 * 
	 * @param enable
	 * @param commodityCode
	 * @return
	 */
	@RequestMapping(value = "/updatecommodityenable", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public JSONObject updatecommodityenable(int enable, String commodityCode) throws UnsupportedEncodingException {
		JSONObject rbody = commodityinfo.updatecommodityenable(enable, commodityCode);
		return rbody;
	}

	/**
	 * 商品删除接口
	 * 
	 * @param commodityCode
	 * @return
	 */
	@RequestMapping(value = "/commodityactivity", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public JSONObject commodityactivity(String commodityCode) throws UnsupportedEncodingException {
		JSONObject rbody = commodityinfo.commodityactivity(commodityCode);
		return rbody;
	}

	/**
	 * 商品修改
	 * 
	 * @param commodity
	 * @return
	 */
	@RequestMapping(value = "/upcommodityInfo", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public JSONObject upcommodityInfo(@RequestBody commodity commodity) throws UnsupportedEncodingException {
		JSONObject rbody = commodityinfo.upcommodityInfo(commodity);
		return rbody;
	}
}
