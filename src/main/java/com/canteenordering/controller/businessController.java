package com.canteenordering.controller;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;

import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.canteenordering.javabean.business;
import com.canteenordering.javabean.notice;
import com.canteenordering.service.businessService;
import com.canteenordering.service.noticeService;
import com.canteenordering.utils.VerifyUtil;
import com.github.pagehelper.PageInfo;

@CrossOrigin(origins = { "*", "null" })
@ResponseBody
@RestController
public class businessController {

	@Autowired
	private businessService businessinfo;

	

	/**
	 * 商家注册
	 * 
	 * @param business
	 * @return
	 */
	@RequestMapping(value = "/insertbusinessInfo", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public JSONObject insertbusinessInfo(@RequestBody business business) throws UnsupportedEncodingException {
		JSONObject rbody = businessinfo.insertbusinessInfo(business);
		return rbody;
	}

	/**
	 * 商家列表查询
	 * 
	 * @param name
	 * @param phone
	 * @param address
	 * @param enable
	 * @param state
	 * @return
	 */
	// @RequestMapping(value = "/getbusinessListInfo", method =
	// RequestMethod.POST, produces = "application/json;charset=UTF-8")
	// public List<business> getbusinessListInfo(String name, String phone,
	// String address, String enable,
	// String state, int page, int pageNum) throws UnsupportedEncodingException
	// {
	// List<business> rbody = businessinfo.getbusinessListInfo(name, phone,
	// address, enable, state, page, pageNum);
	// return rbody;
	// }

	@RequestMapping(value = "/getbusinessListInfo", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public PageInfo<business> getbusinessListInfo(String name, String phone, String address, String enable,
			String state, int page, int pageNum, String isRecommend) throws UnsupportedEncodingException {
		PageInfo<business> rbody = businessinfo.getbusinessListInfo(name, phone, address, enable, state, page, pageNum,
				isRecommend);
		return rbody;
	}

	/**
	 * 商家详情查询
	 * 
	 * @param businessCode
	 * @return
	 */
	@RequestMapping(value = "/getbusinessCodeInfo", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public business getbusinessCodeInfo(String businessCode) throws UnsupportedEncodingException {
		business rbody = businessinfo.getbusinessCodeInfo(businessCode);
		return rbody;
	}

	/**
	 * 商家pc登录
	 * 
	 * @param businessName
	 * @param password
	 * @return
	 */
	@RequestMapping(value = "/loginbusiness", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public business loginbusiness(String businessName, String password) throws UnsupportedEncodingException {
		business rbody = businessinfo.loginbusiness(businessName, password);
		return rbody;
	}

	/**
	 * 商家通过openid登录
	 * 
	 * @param openid
	 * @return
	 */
	@RequestMapping(value = "/openidloginbusiness", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public business openidloginbusiness(String openid) throws UnsupportedEncodingException {
		business rbody = businessinfo.openidloginbusiness(openid);
		return rbody;
	}

	/**
	 * 修改商家营业状态 0 打样 1营业
	 * 
	 * @param state
	 * @param businessCode
	 * @return
	 */
	@RequestMapping(value = "/updatebusinessstate", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public JSONObject updatebusinessstate(int state, String businessCode) throws UnsupportedEncodingException {
		JSONObject rbody = businessinfo.updatebusinessstate(state, businessCode);
		return rbody;
	}

	/**
	 * 商家禁用启用
	 * 
	 * @param enable
	 * @param businessCode
	 * @return
	 */
	@RequestMapping(value = "/updatebusinessenable", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public JSONObject updatebusinessenable(int enable, String businessCode) throws UnsupportedEncodingException {
		JSONObject rbody = businessinfo.updatebusinessenable(enable, businessCode);
		return rbody;
	}

	/**
	 * 设置商家是否推荐
	 * 
	 * @param isRecommend
	 * @param businessCode
	 * @return
	 */
	@RequestMapping(value = "/updatebusinessisRecommend", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public JSONObject updatebusinessisRecommend(int isRecommend, String businessCode)
			throws UnsupportedEncodingException {
		JSONObject rbody = businessinfo.updatebusinessisRecommend(isRecommend, businessCode);
		return rbody;
	}

	/**
	 * 绑定openid
	 * 
	 * @param openid
	 * @param businessName
	 * @param password
	 * @return
	 */
	@RequestMapping(value = "/bindingbusiness", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public JSONObject bindingbusiness(String openid, String businessName, String password)
			throws UnsupportedEncodingException {
		JSONObject rbody = businessinfo.bindingbusiness(openid, businessName, password);
		return rbody;
	}

	/**
	 * 商家删除接口
	 * 
	 * @param businessCode
	 * @return
	 */
	@RequestMapping(value = "/businessactivity", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public JSONObject businessactivity(String businessCode) throws UnsupportedEncodingException {
		JSONObject rbody = businessinfo.businessactivity(businessCode);
		return rbody;
	}

	/**
	 * 修改商家信息
	 * 
	 * @param business
	 * @return
	 */
	@RequestMapping(value = "/upbusinessInfo", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public JSONObject upbusinessInfo(@RequestBody business business) throws UnsupportedEncodingException {
		JSONObject rbody = businessinfo.upbusinessInfo(business);
		return rbody;
	}

	/**
	 * 修改密码
	 * 
	 * @param newpassword
	 * @param businessName
	 * @param oldpassword
	 * @return
	 */
	@RequestMapping(value = "/upbusinesspassword", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public JSONObject upbusinesspassword(String newpassword, String businessName, String oldpassword)
			throws UnsupportedEncodingException {
		JSONObject rbody = businessinfo.upbusinesspassword(newpassword, businessName, oldpassword);
		return rbody;
	}

}
