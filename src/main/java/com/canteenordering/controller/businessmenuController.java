package com.canteenordering.controller;

import java.io.UnsupportedEncodingException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.canteenordering.javabean.business;
import com.canteenordering.javabean.businessmenu;
import com.canteenordering.javabean.notice;
import com.canteenordering.service.businessService;
import com.canteenordering.service.businessmenuService;
import com.canteenordering.service.noticeService;

@CrossOrigin(origins = { "*", "null" })
@ResponseBody
@RestController
public class businessmenuController {

	@Autowired
	private businessmenuService businessmenuinfo;


	/**
	 * 商家添加菜单
	 * @param businessmenu
	 * @return
	 */
	@RequestMapping(value = "/insertbusinessmenu", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public JSONObject insertbusinessmenu(@RequestBody businessmenu businessmenu) throws UnsupportedEncodingException {
		JSONObject rbody = businessmenuinfo.insertbusinessmenu(businessmenu);
		return rbody;
	}
	
	/**
	 * 查询商家菜单
	 * @param businessCode
	 * @return
	 */
	@RequestMapping(value = "/gettbusinessmenuList", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public List<businessmenu> gettbusinessmenuList(String businessCode,int enable)
			throws UnsupportedEncodingException {
		List<businessmenu> rbody = businessmenuinfo.gettbusinessmenuList(businessCode, enable);
		return rbody;
	}
	
	/**
	 * 商家菜单禁用启用
	 * @param enable
	 * @param businessCode
	 * @return
	 */
	@RequestMapping(value = "/updatebusinessmenuenable", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public JSONObject updatebusinessenable(int enable, String menuCode) throws UnsupportedEncodingException {
		JSONObject rbody = businessmenuinfo.updatebusinessmenuenable(enable, menuCode);
		return rbody;
	}

	/**
	 * 商家菜单删除接口
	 * @param menuCode
	 * @return
	 */
	@RequestMapping(value = "/businessmenuactivity", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public JSONObject businessmenuactivity(String menuCode) throws UnsupportedEncodingException {
		JSONObject rbody = businessmenuinfo.businessmenuactivity( menuCode);
		return rbody;
	}
	
	/**
	 * 修改菜单名称
	 * @param menuName
	 * @param menuCode
	 * @return
	 */
	@RequestMapping(value = "/upbusinessmenuName", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public JSONObject upbusinessmenuName(String menuName, String menuCode) throws UnsupportedEncodingException {
		JSONObject rbody = businessmenuinfo.upbusinessmenuName(menuName, menuCode);
		return rbody;
	}
	
	/**
	 * 设置菜单顺序
	 * @param listinfo
	 * @return
	 */
	@RequestMapping(value = "/upbusinessmenunum", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public JSONObject upbusinessmenunum(String json) throws UnsupportedEncodingException {
		JSONObject rbody = businessmenuinfo.upbusinessmenunum(json);
		return rbody;
	}
	

}
