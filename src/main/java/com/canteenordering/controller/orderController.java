package com.canteenordering.controller;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.canteenordering.excel.ExcelDownloadOrder;
import com.canteenordering.javabean.business;
import com.canteenordering.javabean.notice;
import com.canteenordering.javabean.order;
import com.canteenordering.javabean.orderVO1;
import com.canteenordering.service.businessService;
import com.canteenordering.service.noticeService;
import com.canteenordering.service.orderService;
import com.github.pagehelper.PageInfo;

@CrossOrigin(origins = { "*", "null" })
@ResponseBody
@RestController
public class orderController {

	@Autowired
	private orderService orderinfo;

	/**
	 * 订单新增
	 * 
	 * @param order
	 * @return
	 */
	@RequestMapping(value = "/insertorderInfo", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public JSONObject insertorderInfo(@RequestBody order order) throws UnsupportedEncodingException {
		JSONObject rbody = orderinfo.insertorderInfo(order);
		WebSocket.AppointSending(order.getBusinessCode(), "1");
		return rbody;
	}

	/**
	 * 订单列表
	 * 
	 * @param page
	 * @param pageNum
	 * @param state
	 * @param businessCode
	 * @param bphone
	 * @param bname
	 * @param customerCode
	 * @param cname
	 * @param cphone
	 * @return
	 */
	@RequestMapping(value = "/getorderListInfo", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public PageInfo<order> getorderListInfo(int page, int pageNum, String state, String businessCode, String bphone,
			String bname, String customerCode, String cname, String cphone, String stateTime, String endTime)
					throws UnsupportedEncodingException {
		PageInfo<order> rbody = orderinfo.getorderListInfo(page, pageNum, state, businessCode, bphone, bname,
				customerCode, cname, cphone, stateTime, endTime);
		return rbody;
	}

	/**
	 * 下载表格
	 * 
	 * @param state
	 * @param businessCode
	 * @param bphone
	 * @param bname
	 * @param customerCode
	 * @param cname
	 * @param cphone
	 * @return
	 */
	@RequestMapping(value = "/excelDownloadOrder", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public String excelDownloadOrder(String state, String businessCode, String bphone, String bname,
			String customerCode, String cname, String cphone, String stateTime, String endTime) throws IOException {
		List<order> rbody = orderinfo.getorderListInfo(state, businessCode, bphone, bname, customerCode, cname, cphone,
				stateTime, endTime);
		String info = ExcelDownloadOrder.Excel(rbody);
		return info;
	}

	/**
	 * 订单详情
	 * 
	 * @param orderCode
	 * @return
	 */
	@RequestMapping(value = "/getorderInfo", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public order getorderInfo(String orderCode) throws UnsupportedEncodingException {
		order rbody = orderinfo.getorderInfo(orderCode);
		return rbody;
	}

	/**
	 * 修改订单状态
	 * 
	 * @param state
	 * @param orderCode
	 * @return
	 */
	@RequestMapping(value = "/updateorderstate", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public JSONObject updateorderstate(int state, String orderCode, String customerCode)
			throws UnsupportedEncodingException {
		JSONObject rbody = orderinfo.updateorderstate(state, orderCode);
		if (state == 2) {
			WebSocket.AppointSending(customerCode, "1");
		}
		return rbody;
	}

	/**
	 * 催单
	 * 
	 * @param orderCode
	 * @return
	 */
	@RequestMapping(value = "/updateorderreminderNum", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public JSONObject updateorderreminderNum(String orderCode, String businessCode)
			throws UnsupportedEncodingException {
		JSONObject rbody = orderinfo.updateorderreminderNum(orderCode);
		WebSocket.AppointSending(businessCode, "1");
		return rbody;
	}

	/**
	 * 统计图
	 * 
	 * @param year
	 * @param month
	 * @param type
	 * @return
	 */
	@RequestMapping(value = "/getordertongji", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public List<orderVO1> getordertongji(String year, String month, int type, String businessCode)
			throws UnsupportedEncodingException {
		List<orderVO1> rbody = orderinfo.getordertongji(year, month, type, businessCode);
		return rbody;
	}

	@RequestMapping(value = "/send", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public void send(String businessCode) throws UnsupportedEncodingException {
		WebSocket.AppointSending(businessCode, "1");
	}

}
