package com.canteenordering.controller;

import java.io.UnsupportedEncodingException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.canteenordering.javabean.business;
import com.canteenordering.javabean.notice;
import com.canteenordering.javabean.order;
import com.canteenordering.javabean.orderComment;
import com.canteenordering.service.businessService;
import com.canteenordering.service.noticeService;
import com.canteenordering.service.orderCommentService;
import com.canteenordering.service.orderService;
import com.github.pagehelper.PageInfo;

@CrossOrigin(origins = { "*", "null" })
@ResponseBody
@RestController
public class orderCommentController {

	@Autowired
	private orderCommentService orderCommentinfo;

	/**
	 * 新增评论
	 * 
	 * @param orderComment
	 * @return
	 */
	@RequestMapping(value = "/insertorderCommentInfo", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public JSONObject insertorderCommentInfo(@RequestBody orderComment orderComment)
			throws UnsupportedEncodingException {
		JSONObject rbody = orderCommentinfo.insertorderCommentInfo(orderComment);
		return rbody;
	}

	/**
	 * 评论列表
	 * @param page
	 * @param pageNum
	 * @param customerCode
	 * @param businessCode
	 * @param isShow
	 * @param orderCode
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	@RequestMapping(value = "/getordercommentListInfo", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public PageInfo<orderComment> getordercommentListInfo(int page, int pageNum, String customerCode, String businessCode,
			String isShow, String orderCode) throws UnsupportedEncodingException {
		PageInfo<orderComment> rbody = orderCommentinfo.getordercommentListInfo(page, pageNum, customerCode,
				businessCode, isShow, orderCode);
		return rbody;
	}

	/**
	 * 评论详情
	 * 
	 * @param commentCode
	 * @return
	 */
	@RequestMapping(value = "/getorderCommentInfo", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public orderComment getorderCommentInfo(String commentCode,String orderCode) throws UnsupportedEncodingException {
		orderComment rbody = orderCommentinfo.getorderCommentInfo(commentCode,orderCode);
		return rbody;
	}

	/**
	 * 是否显示评论
	 * 
	 * @param isShow
	 * @param commentCode
	 * @return
	 */
	@RequestMapping(value = "/updateorderComment", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public JSONObject updateorderComment(int isShow, String commentCode) throws UnsupportedEncodingException {
		JSONObject rbody = orderCommentinfo.updateorderComment(isShow, commentCode);
		return rbody;
	}

	/**
	 * 
	 * 获取商家好率
	 * @param businessCode
	 * @return
	 */
	@RequestMapping(value = "/getbusinessCommentNum", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public JSONObject getbusinessCommentNum(String businessCode) throws UnsupportedEncodingException {
		JSONObject rbody = orderCommentinfo.getbusinessCommentNum(businessCode);
		return rbody;
	}

}
