package com.canteenordering.controller;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.alibaba.fastjson.JSONObject;
import com.canteenordering.javabean.business;
import com.canteenordering.utils.PublicUtils;
import com.canteenordering.utils.QRCodeUtil;
import com.canteenordering.utils.VerifyUtil;

@CrossOrigin(origins = { "*", "null" })
@ResponseBody
@RestController
public class fileController {

	/**
	 * @author XXXXXX
	 * @date 2018年7月11日
	 * @desc 图形验证码生成
	 */
	@RequestMapping("/createImg")
	public void createImg(HttpServletRequest request, HttpServletResponse response) throws Exception {
		try {
			response.setContentType("image/jpeg");// 设置相应类型,告诉浏览器输出的内容为图片
			response.setHeader("Pragma", "No-cache");// 设置响应头信息，告诉浏览器不要缓存此内容
			response.setHeader("Cache-Control", "no-cache");
			response.setContentType("application/octet-stream;charset=UTF-8");
			response.setHeader("Access-Control-Expose-Headers", "Content-Disposition");
			response.setHeader("Access-Control-Allow-Headers", "*");
			response.setHeader("Access-Control-Allow-Origin", "*");
			response.setDateHeader("Expire", 0);
			VerifyUtil randomValidateCode = new VerifyUtil();
			randomValidateCode.getRandcode(request, response);// 输出验证码图片
		} catch (Exception e) {
		}
	}

	/**
	 * 生成二维码
	 * @param QRURL
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	@RequestMapping(value = "/QRCode", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public String QRCode(String QRURL) throws UnsupportedEncodingException {
		String rbody = QRCodeUtil.getQRCodeBase64(QRURL, 200, 200);
		return rbody;
	}

	//上传
	@PostMapping("/upload")
	@ResponseBody
	public JSONObject upload(@RequestParam("file") MultipartFile file, HttpServletRequest request) {
		if (file.isEmpty()) {
			JSONObject rbody = PublicUtils.getReturn("上传失败，请选择文件", "0");
			return rbody;
		}
		// String path
		// =request.getSession().getServletContext().getRealPath("uploadFile/");
		// String fileName = file.getOriginalFilename();

		long time = System.currentTimeMillis();
		// String path = "F:/1587268162299/";
		String path = "C:/apache-tomcat-7.0.93/webapps/uploadFile/";
		// String path =
		// "C:/apache-tomcat-7.0.93/webapps/canteenordering/WEB-INF/classes/uploadFile";

		File dest = new File(path + time + ".png");
		try {
			file.transferTo(dest);
			JSONObject rjson = new JSONObject();
			rjson.put("flag", 1);
			rjson.put("path", time + ".png");
			rjson.put("fileName", time + ".png");
			return rjson;
		} catch (IOException e) {
			e.printStackTrace();
		}
		JSONObject rbody = PublicUtils.getReturn("上传失败！", "0");
		return rbody;
	}
}
