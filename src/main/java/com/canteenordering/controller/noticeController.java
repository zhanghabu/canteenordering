package com.canteenordering.controller;

import java.io.UnsupportedEncodingException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.canteenordering.javabean.notice;
import com.canteenordering.service.noticeService;

@CrossOrigin(origins = { "*", "null" })
@ResponseBody
@RestController
public class noticeController {

	@Autowired
	private noticeService noticeinfo;

	@RequestMapping(value = "/insertnoticeInfo", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public JSONObject insertnoticeInfo(@RequestBody notice notice) throws UnsupportedEncodingException {
		JSONObject rbody = noticeinfo.insertnoticeInfo(notice);
		return rbody;
	}

	@RequestMapping(value = "/getnoticeListInfo", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public List<notice> getnoticeListInfo() throws UnsupportedEncodingException {
		List<notice> rbody = noticeinfo.getnoticeListInfo();
		return rbody;
	}
	
	@RequestMapping(value = "/getnoticeInfo", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public notice getnoticeInfo(String noticeCode) throws UnsupportedEncodingException {
		notice rbody = noticeinfo.getnoticeInfo(noticeCode);
		return rbody;
	}

	@RequestMapping(value = "/deletenotice", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public JSONObject deletenotice(String noticeCode) throws UnsupportedEncodingException {
		JSONObject rbody = noticeinfo.deletenotice(noticeCode);
		return rbody;
	}

}
