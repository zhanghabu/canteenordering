package com.canteenordering.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;
import com.canteenordering.javabean.LoginInfo;
import com.canteenordering.utils.WXUtils;

@CrossOrigin(origins = { "*", "null" })
@ResponseBody
@RestController
public class oathController {

	String APPID = "wx7677dd58b646116b";
	String SECRET = "ef6e46237b64f796feec697f56075541";
	String GETINFO = "https://api.weixin.qq.com/sns/jscode2session?appid=APPID&secret=SECRET&js_code=JSCODE&grant_type=authorization_code";
	String ACCTOKEN = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=APPID&secret=SECRET";

	@PostMapping("/getopenid")
	@ResponseBody
	public LoginInfo upload(String code) {
		LoginInfo loginInfo = null;

		String requestUrl = GETINFO.replace("APPID", APPID).replace("SECRET", SECRET)
				.replace("JSCODE", code);
		JSONObject jsonObject = WXUtils.httpRequest(requestUrl, "POST", null);
		if (null != jsonObject) {
			try {
				loginInfo = new LoginInfo();
				loginInfo.setOpenid(jsonObject.getString("openid"));
				loginInfo.setSession_key(jsonObject.getString("session_key"));
			} catch (JSONException e) {
				loginInfo = null;
				System.out.println("获取用户信息失败 " + jsonObject.getInteger("errcode") + jsonObject.getString("errmsg"));
			}
		}
		return loginInfo;
	}
}
