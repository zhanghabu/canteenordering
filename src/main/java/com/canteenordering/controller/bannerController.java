package com.canteenordering.controller;

import java.io.UnsupportedEncodingException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.canteenordering.javabean.banner;
import com.canteenordering.javabean.business;
import com.canteenordering.javabean.notice;
import com.canteenordering.service.bannerService;
import com.canteenordering.service.businessService;
import com.canteenordering.service.noticeService;
import com.github.pagehelper.PageInfo;

@CrossOrigin(origins = { "*", "null" })
@ResponseBody
@RestController
public class bannerController {

	@Autowired
	private bannerService bannerinfo;

	/**
	 * banner新增
	 * @param banner
	 * @return
	 */
	@RequestMapping(value = "/insertbannerInfo", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public JSONObject insertbannerInfo(@RequestBody banner banner) throws UnsupportedEncodingException {
		JSONObject rbody = bannerinfo.insertbannerInfo(banner);
		return rbody;
	}
	/**
	 * banner列表
	 * @param bannerType
	 * @param enable
	 * @return
	 */
	@RequestMapping(value = "/getbannerListInfo", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public List<banner> getbannerListInfo(String bannerType,String enable) throws UnsupportedEncodingException {
		List<banner> rbody = bannerinfo.getbannerListInfo(bannerType, enable);
		return rbody;
	}
	
	/**
	 * banner禁用启用
	 * @param enable
	 * @param bannerCode
	 * @return
	 */
	@RequestMapping(value = "/bannerenable", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public JSONObject bannerenable(int enable, String bannerCode) throws UnsupportedEncodingException {
		JSONObject rbody = bannerinfo.bannerenable(enable, bannerCode);
		return rbody;
	}
	/**
	 * banner删除
	 * @param bannerCode
	 * @return
	 */
	@RequestMapping(value = "/banneractivity", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public JSONObject banneractivity(String bannerCode) throws UnsupportedEncodingException {
		JSONObject rbody = bannerinfo.banneractivity(bannerCode);
		return rbody;
	}
}
