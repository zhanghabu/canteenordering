package com.canteenordering.javabean;

import java.util.List;

public class order {

	private String id;
	private String orderCode;
	private int state;
	private String businessCode;
	private String bname;
	private String bphone;
	private String sumPrice;
	private String enable;
	private String remarks;
	private String customerCode;
	private String cname;
	private String cphone;
	private int reminderNum;
	private String addTime;
	private String businessPhoto;
	private String customerPhoto;
	
	private String isComment;

	
	
	public String getIsComment() {
		return isComment;
	}
	public void setIsComment(String isComment) {
		this.isComment = isComment;
	}
	public String getAddTime() {
		return addTime;
	}
	public void setAddTime(String addTime) {
		this.addTime = addTime;
	}
	public String getBusinessPhoto() {
		return businessPhoto;
	}
	public void setBusinessPhoto(String businessPhoto) {
		this.businessPhoto = businessPhoto;
	}
	public String getCustomerPhoto() {
		return customerPhoto;
	}
	public void setCustomerPhoto(String customerPhoto) {
		this.customerPhoto = customerPhoto;
	}
	private List<orderCommodity>  orderCommodity;

	
	
	
	@Override
	public String toString() {
		return "order [id=" + id + ", orderCode=" + orderCode + ", state=" + state + ", businessCode=" + businessCode
				+ ", bname=" + bname + ", bphone=" + bphone + ", sumPrice=" + sumPrice + ", enable=" + enable
				+ ", remarks=" + remarks + ", customerCode=" + customerCode + ", cname=" + cname + ", cphone=" + cphone
				+ ", reminderNum=" + reminderNum + ", orderCommodity=" + orderCommodity + "]";
	}
	public List<orderCommodity> getOrderCommodity() {
		return orderCommodity;
	}
	public void setOrderCommodity(List<orderCommodity> orderCommodity) {
		this.orderCommodity = orderCommodity;
	}
	public int getState() {
		return state;
	}
	public void setState(int state) {
		this.state = state;
	}
	public int getReminderNum() {
		return reminderNum;
	}
	public void setReminderNum(int reminderNum) {
		this.reminderNum = reminderNum;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getOrderCode() {
		return orderCode;
	}
	public void setOrderCode(String orderCode) {
		this.orderCode = orderCode;
	}
	public String getBusinessCode() {
		return businessCode;
	}
	public void setBusinessCode(String businessCode) {
		this.businessCode = businessCode;
	}
	public String getBname() {
		return bname;
	}
	public void setBname(String bname) {
		this.bname = bname;
	}
	public String getBphone() {
		return bphone;
	}
	public void setBphone(String bphone) {
		this.bphone = bphone;
	}
	public String getSumPrice() {
		return sumPrice;
	}
	public void setSumPrice(String sumPrice) {
		this.sumPrice = sumPrice;
	}
	public String getEnable() {
		return enable;
	}
	public void setEnable(String enable) {
		this.enable = enable;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public String getCustomerCode() {
		return customerCode;
	}
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	public String getCname() {
		return cname;
	}
	public void setCname(String cname) {
		this.cname = cname;
	}
	public String getCphone() {
		return cphone;
	}
	public void setCphone(String cphone) {
		this.cphone = cphone;
	}
}
