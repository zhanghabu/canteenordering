package com.canteenordering.javabean;

public class banner {

	
	private String id;
	private String bannerCode;
	private int bannerType;
	private String bannerName;
	private String enable;
	private String addTime;
	private int num;
	private String bannerPic;
	private String bannerInfo;
	private String businessCode;
	private String commodityCode;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getBannerCode() {
		return bannerCode;
	}
	public void setBannerCode(String bannerCode) {
		this.bannerCode = bannerCode;
	}
	public int getBannerType() {
		return bannerType;
	}
	public void setBannerType(int bannerType) {
		this.bannerType = bannerType;
	}
	public String getBannerName() {
		return bannerName;
	}
	public void setBannerName(String bannerName) {
		this.bannerName = bannerName;
	}
	public String getEnable() {
		return enable;
	}
	public void setEnable(String enable) {
		this.enable = enable;
	}
	public String getAddTime() {
		return addTime;
	}
	public void setAddTime(String addTime) {
		this.addTime = addTime;
	}
	public int getNum() {
		return num;
	}
	public void setNum(int num) {
		this.num = num;
	}
	public String getBannerPic() {
		return bannerPic;
	}
	public void setBannerPic(String bannerPic) {
		this.bannerPic = bannerPic;
	}
	public String getBannerInfo() {
		return bannerInfo;
	}
	public void setBannerInfo(String bannerInfo) {
		this.bannerInfo = bannerInfo;
	}
	public String getBusinessCode() {
		return businessCode;
	}
	public void setBusinessCode(String businessCode) {
		this.businessCode = businessCode;
	}
	public String getCommodityCode() {
		return commodityCode;
	}
	public void setCommodityCode(String commodityCode) {
		this.commodityCode = commodityCode;
	}
	
	
	
	
	
}
