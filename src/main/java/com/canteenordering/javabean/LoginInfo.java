package com.canteenordering.javabean;
/**
 *Copyright © 2018希霆云梦. All rights reserved.
 *
 *@Prject miniprogram
 *@Title LoginInfo.java
 *@date 2018年4月11日 下午4:16:25
 *@author ZhuPengYan
 *@version: v1.0 
 */
public class LoginInfo {

	private String openid;
	private String session_key;
	
	public String getOpenid() {
		return openid;
	}
	public void setOpenid(String openid) {
		this.openid = openid;
	}
	public String getSession_key() {
		return session_key;
	}
	public void setSession_key(String session_key) {
		this.session_key = session_key;
	}
}
