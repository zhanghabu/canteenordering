package com.canteenordering.javabean;

public class orderCommodity {

	private String id;
	private String orderCode;
	private String commodityCode;
	private String commodityName;
	private String num;
	private String commodityphoto;
	private String price;
	private String enable;
	private String addTime;
	

	@Override
	public String toString() {
		return "orderCommodity [id=" + id + ", orderCode=" + orderCode + ", commodityCode=" + commodityCode
				+ ", commodityName=" + commodityName + ", num=" + num + ", commodityphoto=" + commodityphoto
				+ ", price=" + price + ", enable=" + enable + ", addTime=" + addTime + "]";
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getOrderCode() {
		return orderCode;
	}
	public void setOrderCode(String orderCode) {
		this.orderCode = orderCode;
	}
	public String getCommodityCode() {
		return commodityCode;
	}
	public void setCommodityCode(String commodityCode) {
		this.commodityCode = commodityCode;
	}
	public String getCommodityName() {
		return commodityName;
	}
	public void setCommodityName(String commodityName) {
		this.commodityName = commodityName;
	}
	public String getNum() {
		return num;
	}
	public void setNum(String num) {
		this.num = num;
	}
	public String getCommodityphoto() {
		return commodityphoto;
	}
	public void setCommodityphoto(String commodityphoto) {
		this.commodityphoto = commodityphoto;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getEnable() {
		return enable;
	}
	public void setEnable(String enable) {
		this.enable = enable;
	}
	public String getAddTime() {
		return addTime;
	}
	public void setAddTime(String addTime) {
		this.addTime = addTime;
	}
	
}
