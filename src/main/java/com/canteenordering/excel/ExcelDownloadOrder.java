package com.canteenordering.excel;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.canteenordering.javabean.order;
import com.github.pagehelper.PageInfo;

import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CellStyle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

public class ExcelDownloadOrder {
	static Logger logger = LoggerFactory.getLogger(ExcelDownloadOrder.class);

	public static String Excel(List<order> result) throws IOException {
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes())
				.getRequest();
		HSSFWorkbook wb = new HSSFWorkbook();
		HSSFSheet sheet = wb.createSheet();
		sheet.setDefaultColumnWidth(11);
		sheet.setDefaultRowHeightInPoints(20.0F);
		HSSFRow row = sheet.createRow(0);
		HSSFCell cells = row.createCell(0);
		cells.setCellValue("订单号");
		cells = row.createCell(1);
		cells.setCellValue("用户名称");
		cells = row.createCell(2);
		cells.setCellValue("用户手机号");
		cells = row.createCell(3);
		cells.setCellValue("商品总价格");
		cells = row.createCell(4);
		cells.setCellValue("备注");
		cells = row.createCell(5);
		cells.setCellValue("下单时间");
		int num=0;
		for (order info : result) {
			String cname = info.getCname();
			String cphone = info.getCphone();
			String orderCode = info.getOrderCode();
			String remarks = info.getRemarks();
			String sumPrice = info.getSumPrice();
			String addTime = info.getAddTime();
			row = sheet.createRow(num+1);
			cells = row.createCell(0);
			cells.setCellValue(orderCode);
			cells = row.createCell(1);
			cells.setCellValue(cname);
			cells = row.createCell(2);
			cells.setCellValue(cphone);
			cells = row.createCell(3);
			cells.setCellValue(sumPrice);
			cells = row.createCell(4);
			cells.setCellValue(remarks);
			cells = row.createCell(5);
			cells.setCellValue(addTime);
			num++;
		}

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();
		String nowDate = sdf.format(date);
		String excelname = nowDate+"订单表";
		excelname = excelname + ".xls";
		logger.info("excelname:{}", excelname);
//		String path = request.getSession().getServletContext().getRealPath("/clauses/questionDownload/");
		String path = "C:/apache-tomcat-7.0.93/webapps/uploadFile/";
//		String path = "E:/";

		logger.info("将要写入的路径path:{}", path);
		FileOutputStream out = new FileOutputStream(path + excelname);
		wb.write(out);
		out.close();
		return excelname;
	}

}

