package com.canteenordering.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.alibaba.fastjson.JSONObject;
import com.canteenordering.dao.businessMapper;
import com.canteenordering.dao.orderCommentMapper;
import com.canteenordering.dao.orderMapper;
import com.canteenordering.javabean.business;
import com.canteenordering.javabean.order;
import com.canteenordering.javabean.orderComment;
import com.canteenordering.javabean.orderCommodity;
import com.canteenordering.utils.PublicUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

@Service
public class orderCommentService {

	@Autowired
	private orderCommentMapper orderCommentinfo;

	/**
	 * 新增评论
	 * 
	 * @param orderComment
	 * @return
	 */
	@Transactional // 添加事务.
	public JSONObject insertorderCommentInfo(orderComment orderComment) {

		JSONObject rbody = null;
		String code = PublicUtils.getRandomString(20);
		orderComment.setCommentCode(code);
		int index = orderCommentinfo.insertorderCommentInfo(orderComment);
		if (index > 0) {
			/**
			 * 修改是否已评论
			 */
			orderCommentinfo.updateorderisComment(orderComment.getOrderCode());
			rbody = PublicUtils.getReturn("新增成功", "1");
		} else {
			rbody = PublicUtils.getReturn("新增失败", "0");
		}
		return rbody;
	}

	/**
	 * 评论列表
	 * 
	 * @param customerCode
	 * @param businessCode
	 * @param isShow
	 * @param orderCode
	 * @return
	 */
	@Transactional // 添加事务.
	public PageInfo<orderComment> getordercommentListInfo(int page, int pageNum, String customerCode,
			String businessCode, String isShow, String orderCode) {
		PageHelper.startPage(page, pageNum);
		List<orderComment> body = orderCommentinfo.getordercommentListInfo(customerCode, businessCode, isShow,
				orderCode);
		PageInfo<orderComment> info = new PageInfo<>(body);
		return info;
	}

	/**
	 * 
	 * 获取商家好率
	 * 
	 * @param businessCode
	 * @return
	 */
	@Transactional // 添加事务.
	public JSONObject getbusinessCommentNum(String businessCode) {
		List<orderComment> body = orderCommentinfo.getordercommentListInfo("", businessCode, "", "");
		int sum = body.size() * 5;
		int cont = 0;
		for (orderComment orderComment : body) {
			cont += orderComment.getCommentNum();
		}
		JSONObject rjson = new JSONObject();
		rjson.put("sum", sum);
		rjson.put("cont", cont);
		return rjson;
	}

	/**
	 * 评论详情
	 * 
	 * @param commentCode
	 * @return
	 */
	@Transactional // 添加事务.
	public orderComment getorderCommentInfo(String commentCode,String orderCode) {
		
		orderComment body=null;
		if(!StringUtils.isEmpty(commentCode)){
			body = orderCommentinfo.getorderCommentInfo(commentCode);
		}
		if(!StringUtils.isEmpty(orderCode)){
			body = orderCommentinfo.getorderCodeInfo(orderCode);
		}
		
		return body;
	}

	/**
	 * 是否显示评论
	 * 
	 * @param isShow
	 * @param commentCode
	 * @return
	 */
	@Transactional // 添加事务.
	public JSONObject updateorderComment(int isShow, String commentCode) {
		JSONObject rbody = null;
		int body = orderCommentinfo.updateorderComment(isShow, commentCode);
		if (body > 0) {
			rbody = PublicUtils.getReturn("修改成功", "1");
		} else {
			rbody = PublicUtils.getReturn("修改失败", "0");
		}
		return rbody;
	}

}
