package com.canteenordering.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.alibaba.fastjson.JSONObject;
import com.canteenordering.dao.businessMapper;
import com.canteenordering.dao.customerMapper;
import com.canteenordering.javabean.business;
import com.canteenordering.utils.PublicUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

@Service
public class businessService {

	@Autowired
	private businessMapper businessinfo;

	@Autowired
	private customerMapper customerinfo;

	/**
	 * 商家注册
	 * 
	 * @param business
	 * @return
	 */
	@Transactional // 添加事务.
	public JSONObject insertbusinessInfo(business business) {

		JSONObject rbody = null;
		// 查询数据库有没有重复账号
		int businessname = businessinfo.getbusinessName(business.getBusinessName());
		if (businessname > 0) {
			rbody = PublicUtils.getReturn("该用户已被注册", "0");
			return rbody;
		}

		// 查询数据库有没有重复账号
		int customerName = customerinfo.getcustomerName(business.getBusinessName());
		if (customerName > 0) {
			rbody = PublicUtils.getReturn("该手机号已被注册用户请更换手机号", "0");
			return rbody;
		}

		String code = PublicUtils.getRandomString(20);
		business.setBusinessCode(code);

		int index = businessinfo.insertbusinessInfo(business);
		if (index > 0) {
			rbody = PublicUtils.getReturn("新增成功", "1");
		} else {
			rbody = PublicUtils.getReturn("新增失败", "0");
		}
		return rbody;
	}

	/**
	 * 商家列表查询
	 * 
	 * @param name
	 * @param phone
	 * @param address
	 * @param enable
	 * @param state
	 * @return
	 */
	// @Transactional // 添加事务.
	// public List<business> getbusinessListInfo(String name,
	// String phone, String address, String enable,
	// String state,int page,int pageNum) {
	// List<business> body = businessinfo.getbusinessListInfo(name, phone,
	// address, enable, state);
	//
	// return body;
	// }
	@Transactional // 添加事务.
	public PageInfo<business> getbusinessListInfo(String name, String phone, String address, String enable,
			String state, int page, int pageNum,String isRecommend) {
		PageHelper.startPage(page, pageNum);
		List<business> body = businessinfo.getbusinessListInfo(name, phone, address, enable, state,isRecommend);
		PageInfo<business> info = new PageInfo<>(body);

		return info;
	}

	/**
	 * 商家详情查询
	 * 
	 * @param businessCode
	 * @return
	 */
	@Transactional // 添加事务.
	public business getbusinessCodeInfo(String businessCode) {
		JSONObject rbody = null;
		business body = businessinfo.getbusinessCodeInfo(businessCode);
		return body;
	}

	/**
	 * 商家pc登录
	 * 
	 * @param businessName
	 * @param password
	 * @return
	 */
	@Transactional // 添加事务.
	public business loginbusiness(String businessName, String password) {
		JSONObject rbody = null;
		business body = businessinfo.loginbusiness(businessName, password);
		return body;
	}

	/**
	 * 商家通过openid登录
	 * 
	 * @param openid
	 * @return
	 */
	@Transactional // 添加事务.
	public business openidloginbusiness(String openid) {
		JSONObject rbody = null;
		business body = businessinfo.openidloginbusiness(openid);

		return body;
	}

	/**
	 * 修改商家营业状态 0 打样 1营业
	 * 
	 * @param state
	 * @param businessCode
	 * @return
	 */
	@Transactional // 添加事务.
	public JSONObject updatebusinessstate(int state, String businessCode) {
		JSONObject rbody = null;
		int body = businessinfo.updatebusinessstate(state, businessCode);
		if (body > 0) {
			rbody = PublicUtils.getReturn("修改成功", "1");
		} else {
			rbody = PublicUtils.getReturn("修改失败", "0");
		}
		return rbody;
	}

	/**
	 * 商家禁用启用
	 * 
	 * @param enable
	 * @param businessCode
	 * @return
	 */
	@Transactional // 添加事务.
	public JSONObject updatebusinessenable(int enable, String businessCode) {
		JSONObject rbody = null;
		int body = businessinfo.updatebusinessenable(enable, businessCode);
		if (body > 0) {
			rbody = PublicUtils.getReturn("修改成功", "1");
		} else {
			rbody = PublicUtils.getReturn("修改失败", "0");
		}
		return rbody;
	}

	/**
	 * 设置商家是否推荐
	 * 
	 * @param isRecommend
	 * @param businessCode
	 * @return
	 */
	@Transactional // 添加事务.
	public JSONObject updatebusinessisRecommend(int isRecommend, String businessCode) {
		JSONObject rbody = null;
		int body = businessinfo.updatebusinessisRecommend(isRecommend, businessCode);
		if (body > 0) {
			rbody = PublicUtils.getReturn("修改成功", "1");
		} else {
			rbody = PublicUtils.getReturn("修改失败", "0");
		}
		return rbody;
	}

	/**
	 * 绑定openid
	 * 
	 * @param openid
	 * @param businessName
	 * @param password
	 * @return
	 */
	@Transactional // 添加事务.
	public JSONObject bindingbusiness(String openid, String businessName, String password) {
		JSONObject rbody = null;
		int index = businessinfo.getopenid(openid);
		if (index > 0) {
			rbody = PublicUtils.getReturn("该用户已绑定", "0");
			return rbody;
		} else {
			index = customerinfo.getopenid(openid);
			if (index > 0) {
				rbody = PublicUtils.getReturn("该身份已绑定用户身份", "0");
				return rbody;
			}
			int body = businessinfo.bindingbusiness(openid, businessName, password);
			if (body > 0) {
				rbody = PublicUtils.getReturn("绑定成功", "1");
			} else {
				rbody = PublicUtils.getReturn("绑定失败", "0");
			}
		}

		return rbody;
	}

	/**
	 * 商家删除接口
	 * 
	 * @param businessCode
	 * @return
	 */
	@Transactional // 添加事务.
	public JSONObject businessactivity(String businessCode) {
		JSONObject rbody = null;
		int body = businessinfo.businessactivity(businessCode);
		if (body > 0) {
			rbody = PublicUtils.getReturn("删除成功", "1");
		} else {
			rbody = PublicUtils.getReturn("删除失败", "0");
		}
		return rbody;
	}

	/**
	 * 修改商家信息
	 * 
	 * @param business
	 * @return
	 */
	@Transactional // 添加事务.
	public JSONObject upbusinessInfo(business business) {
		JSONObject rbody = null;
		int body = businessinfo.upbusinessInfo(business);
		if (body > 0) {
			rbody = PublicUtils.getReturn("修改成功", "1");
		} else {
			rbody = PublicUtils.getReturn("修改失败", "0");
		}
		return rbody;
	}

	/**
	 * 修改密码
	 * 
	 * @param newpassword
	 * @param businessName
	 * @param oldpassword
	 * @return
	 */
	@Transactional // 添加事务.
	public JSONObject upbusinesspassword(String newpassword, String businessName, String oldpassword) {
		JSONObject rbody = null;
		int body = businessinfo.upbusinesspassword(newpassword, businessName, oldpassword);
		if (body > 0) {
			rbody = PublicUtils.getReturn("修改成功", "1");
		} else {
			rbody = PublicUtils.getReturn("修改失败", "0");
		}
		return rbody;
	}

}
