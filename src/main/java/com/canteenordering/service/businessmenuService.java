package com.canteenordering.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.canteenordering.dao.businessMapper;
import com.canteenordering.dao.businessmenuMapper;
import com.canteenordering.javabean.business;
import com.canteenordering.javabean.businessmenu;
import com.canteenordering.utils.PublicUtils;

@Service
public class businessmenuService {

	@Autowired
	private businessmenuMapper businessmenuinfo;

	/**
	 * 商家添加菜单
	 * @param business
	 * @return
	 */
	@Transactional // 添加事务.
	public JSONObject insertbusinessmenu(businessmenu businessmenu) {

		JSONObject rbody = null;
		String Name=businessmenuinfo.gettmenuName(businessmenu.getBusinessCode(),businessmenu.getMenuName());
		System.out.println("Name:"+Name);
		if(!StringUtils.isEmpty(Name)){
			rbody = PublicUtils.getReturn("名字重复", "0");
			return rbody;
		}
		String code = PublicUtils.getRandomString(20);
		businessmenu.setMenuCode(code);
		int index = businessmenuinfo.insertbusinessmenu(businessmenu);
		if (index > 0) {
			rbody = PublicUtils.getReturn("新增成功", "1");
		} else {
			rbody = PublicUtils.getReturn("新增失败", "0");
		}
		return rbody;
	}

	/**
	 * 查询商家菜单
	 * @param businessCode
	 * @return
	 */
	@Transactional // 添加事务.
	public List<businessmenu> gettbusinessmenuList(String businessCode,int enable) {
		JSONObject rbody = null;
		List<businessmenu> body = businessmenuinfo.gettbusinessmenuList(businessCode,enable);
		return body;
	}

	/**
	 * 商家菜单禁用启用
	 * @param enable
	 * @param businessCode
	 * @return
	 */
	@Transactional // 添加事务.
	public JSONObject updatebusinessmenuenable(int enable, String menuCode) {
		JSONObject rbody = null;
		int body = businessmenuinfo.updatebusinessmenuenable(enable, menuCode);
		if (body > 0) {
			rbody = PublicUtils.getReturn("修改成功", "1");
		} else {
			rbody = PublicUtils.getReturn("修改失败", "0");
		}
		return rbody;
	}
	
	
	/**
	 * 商家菜单删除接口
	 * @param menuCode
	 * @return
	 */
	@Transactional // 添加事务.
	public JSONObject businessmenuactivity(String menuCode) {
		JSONObject rbody = null;
		int body = businessmenuinfo.businessmenuactivity(menuCode);
		if (body > 0) {
			rbody = PublicUtils.getReturn("删除成功", "1");
		} else {
			rbody = PublicUtils.getReturn("删除失败", "0");
		}
		return rbody;
	}
	
	/**
	 * 修改菜单名称
	 * @param menuName
	 * @param menuCode
	 * @return
	 */
	@Transactional // 添加事务.
	public JSONObject upbusinessmenuName(String menuName, String menuCode) {
		JSONObject rbody = null;
		int body = businessmenuinfo.upbusinessmenuName(menuName, menuCode);
		if (body > 0) {
			rbody = PublicUtils.getReturn("修改成功", "1");
		} else {
			rbody = PublicUtils.getReturn("修改失败", "0");
		}
		return rbody;
	}
	
	/**
	 * 设置菜单书序
	 * @param listinfo
	 * @return
	 */
	@Transactional // 添加事务.
	public JSONObject upbusinessmenunum(String listinfo) {
		JSONObject rbody = null;
		int body=0;
		JSONArray list = JSONArray.parseArray(listinfo);
		for (Object object : list) {
			JSONObject info = JSONObject.parseObject(object.toString());
			String menuCode=info.getString("menuCode");
			int num =info.getInteger("num");
			body+=businessmenuinfo.upbusinessmenunum(num, menuCode);
		}
		if (body > 0) {
			rbody = PublicUtils.getReturn("修改成功", "1");
		} else {
			rbody = PublicUtils.getReturn("修改失败", "0");
		}
		return rbody;
	}

}
