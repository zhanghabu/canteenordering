package com.canteenordering.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.alibaba.fastjson.JSONObject;
import com.canteenordering.dao.bannerMapper;
import com.canteenordering.dao.businessMapper;
import com.canteenordering.dao.customerMapper;
import com.canteenordering.javabean.banner;
import com.canteenordering.javabean.business;
import com.canteenordering.utils.PublicUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

@Service
public class bannerService {

	@Autowired
	private bannerMapper bannerinfo;
	

	/**
	 * banner新增
	 * 
	 * @param banner
	 * @return
	 */
	@Transactional // 添加事务.
	public JSONObject insertbannerInfo(banner banner) {
		
		JSONObject rbody = null;
		String code = PublicUtils.getRandomString(20);
		banner.setBannerCode(code);
		int index = bannerinfo.insertbannerInfo(banner);
		if (index > 0) {
			rbody = PublicUtils.getReturn("新增成功", "1");
		} else {
			rbody = PublicUtils.getReturn("新增失败", "0");
		}
		return rbody;
	}

	/**
	 * banner列表
	 * @param bannerType
	 * @param enable
	 * @return
	 */
	@Transactional // 添加事务.
	public List<banner> getbannerListInfo(String bannerType,String enable) {
		List<banner> body = bannerinfo.getbannerListInfo(bannerType, enable);
		return body;
	}
	

	/**
	 * banner禁用启用
	 * @param enable
	 * @param bannerCode
	 * @return
	 */
	@Transactional // 添加事务.
	public JSONObject bannerenable(int enable, String bannerCode) {
		JSONObject rbody = null;
		int body = bannerinfo.bannerenable(enable, bannerCode);
		if (body > 0) {
			rbody = PublicUtils.getReturn("修改成功", "1");
		} else {
			rbody = PublicUtils.getReturn("修改失败", "0");
		}
		return rbody;
	}
	
	/**
	 * banner删除
	 * @param bannerCode
	 * @return
	 */
	@Transactional // 添加事务.
	public JSONObject banneractivity(String bannerCode) {
		JSONObject rbody = null;
		int body = bannerinfo.banneractivity(bannerCode);
		if (body > 0) {
			rbody = PublicUtils.getReturn("删除成功", "1");
		} else {
			rbody = PublicUtils.getReturn("删除失败", "0");
		}
		return rbody;
	}

}
