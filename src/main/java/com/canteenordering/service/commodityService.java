package com.canteenordering.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.alibaba.fastjson.JSONObject;
import com.canteenordering.dao.businessMapper;
import com.canteenordering.dao.commodityMapper;
import com.canteenordering.javabean.business;
import com.canteenordering.javabean.commodity;
import com.canteenordering.utils.PublicUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

@Service
public class commodityService {

	@Autowired
	private commodityMapper commodityinfo;

	/**
	 * 商品新增
	 * 
	 * @param commodity
	 * @return
	 */
	@Transactional // 添加事务.
	public JSONObject insertcommodity(commodity commodity) {

		JSONObject rbody = null;

		String Name = commodityinfo.getcommodityName(commodity.getMenuCode(), commodity.getCommodityName());
		if (!StringUtils.isEmpty(Name)) {
			rbody = PublicUtils.getReturn("名字重复", "0");
			return rbody;
		}
		String code = PublicUtils.getRandomString(20);
		commodity.setCommodityCode(code);
		int index = commodityinfo.insertcommodity(commodity);
		if (index > 0) {
			rbody = PublicUtils.getReturn("新增成功", "1");
		} else {
			rbody = PublicUtils.getReturn("新增失败", "0");
		}
		return rbody;
	}

	/**
	 * 查询商品列表
	 * 
	 * @param commodityName
	 * @param commodityContent
	 * @param menuCode
	 * @param enable
	 * @param businessCode
	 * @return
	 */
	@Transactional // 添加事务.
	public PageInfo<commodity> getcommodityListInfo(String commodityName, String commodityContent, String menuCode,
			String enable, String businessCode, int page, int pageNum) {
		PageHelper.startPage(page, pageNum);
		List<commodity> body = commodityinfo.getcommodityListInfo(commodityName, commodityContent, menuCode, enable,
				businessCode);
		for (commodity commodity : body) {
			String name = commodityinfo.getbusinessCodeInfo(commodity.getBusinessCode());
			String menuName = commodityinfo.getbusinessmenuInfo(commodity.getMenuCode());
			System.out.println("getCommodityCode:" + commodity.getCommodityCode());
			List<Integer> commoditynum = commodityinfo.getcommoditynum(commodity.getCommodityCode());
			int num=0;
			for (Integer integer : commoditynum) {
				num=num+integer;
			}
			System.out.println("commoditynum:" + commoditynum);
			commodity.setMenuName(menuName);
			commodity.setName(name);
			commodity.setCommodityNum(num + "");
		}
		PageInfo<commodity> info = new PageInfo<commodity>(body);
		return info;
	}

	/**
	 * 商品详情查询
	 * 
	 * @param commodityCode
	 * @return
	 */
	@Transactional // 添加事务.
	public commodity getcommodityInfo(String commodityCode) {
		JSONObject rbody = null;
		commodity body = commodityinfo.getcommodityInfo(commodityCode);
		String name = commodityinfo.getbusinessCodeInfo(body.getBusinessCode());
		String menuName = commodityinfo.getbusinessmenuInfo(body.getMenuCode());
		body.setMenuName(menuName);
		body.setName(name);
		return body;
	}

	/**
	 * 商品禁用启用
	 * 
	 * @param enable
	 * @param commodityCode
	 * @return
	 */
	@Transactional // 添加事务.
	public JSONObject updatecommodityenable(int enable, String commodityCode) {
		JSONObject rbody = null;
		int body = commodityinfo.updatecommodityenable(enable, commodityCode);
		if (body > 0) {
			rbody = PublicUtils.getReturn("修改成功", "1");
		} else {
			rbody = PublicUtils.getReturn("修改失败", "0");
		}
		return rbody;
	}

	/**
	 * 商品删除接口
	 * 
	 * @param commodityCode
	 * @return
	 */
	@Transactional // 添加事务.
	public JSONObject commodityactivity(String commodityCode) {
		JSONObject rbody = null;
		int body = commodityinfo.commodityactivity(commodityCode);
		if (body > 0) {
			rbody = PublicUtils.getReturn("删除成功", "1");
		} else {
			rbody = PublicUtils.getReturn("删除失败", "0");
		}
		return rbody;
	}

	/**
	 * 商品修改
	 * 
	 * @param commodity
	 * @return
	 */
	@Transactional // 添加事务.
	public JSONObject upcommodityInfo(commodity commodity) {
		JSONObject rbody = null;
		int body = commodityinfo.upcommodityInfo(commodity);
		if (body > 0) {
			rbody = PublicUtils.getReturn("修改成功", "1");
		} else {
			rbody = PublicUtils.getReturn("修改失败", "0");
		}
		return rbody;
	}

}
