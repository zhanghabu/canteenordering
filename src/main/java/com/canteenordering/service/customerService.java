package com.canteenordering.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.alibaba.fastjson.JSONObject;
import com.canteenordering.dao.businessMapper;
import com.canteenordering.dao.customerMapper;
import com.canteenordering.javabean.business;
import com.canteenordering.javabean.customer;
import com.canteenordering.utils.PublicUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

@Service
public class customerService {

	@Autowired
	private customerMapper customerinfo;
	
	@Autowired
	private businessMapper businessinfo;

	/**
	 * 用户注册
	 * @param customer
	 * @return
	 */
	@Transactional // 添加事务.
	public JSONObject insertcustomerInfo(customer customer) {
		
		JSONObject rbody = null;
		// 查询数据库有没有重复账号
		int customerName=customerinfo.getcustomerName(customer.getCustomerName());
		if(customerName>0){
			rbody = PublicUtils.getReturn("该用户已被注册", "0");
			return rbody;
		}
		
		// 查询数据库有没有重复账号
		int businessname=businessinfo.getbusinessName(customer.getCustomerName());
		if(businessname>0){
			rbody = PublicUtils.getReturn("该手机号已被注册成商家请更换手机号", "0");
			return rbody;
		}
		
		String code = PublicUtils.getRandomString(20);
		customer.setCustomerCode(code);
		
		int index = customerinfo.insertcustomerInfo(customer);
		if (index > 0) {
			rbody = PublicUtils.getReturn("新增成功", "1");
		} else {
			rbody = PublicUtils.getReturn("新增失败", "0");
		}
		return rbody;
	}

	/**
	 * 用户列表查询
	 * @param name
	 * @param phone
	 * @param enable
	 * @return
	 */
	@Transactional // 添加事务.
	public PageInfo<customer> getcustomerListInfo(String name, String phone,String enable,int page,int pageNum) {
		PageHelper.startPage(page, pageNum);
		List<customer> body = customerinfo.getcustomerListInfo(name, phone, enable);
		System.out.println(body.get(0));
		PageInfo<customer> info = new PageInfo<>(body);
		return info;
	}
	

	/**
	 * 用户详情
	 * @param customerCode
	 * @return
	 */
	@Transactional // 添加事务.
	public customer getcustomerCodeInfo(String customerCode) {
		JSONObject rbody = null;
		customer body = customerinfo.getcustomerCodeInfo(customerCode);
		return body;
	}

	

	/**
	 * 用户通过openid登录
	 * 
	 * @param openid
	 * @return
	 */
	@Transactional // 添加事务.
	public customer openidlogincustomer(String openid) {
		customer body = customerinfo.openidlogincustomer(openid);
		return body;
	}

	/**
	 * 绑定openid
	 * @param openid
	 * @param customerName
	 * @param password
	 * @return
	 */
	@Transactional // 添加事务.
	public JSONObject bindingcustomer(String openid, String customerName, String password) {
		JSONObject rbody = null;
		int index=customerinfo.getopenid(openid);
		if(index>0){
			rbody = PublicUtils.getReturn("该用户已绑定", "0");
			return rbody;
		}else{
			int body = customerinfo.bindingcustomer(openid, customerName, password);
			if (body > 0) {
				rbody = PublicUtils.getReturn("绑定成功", "1");
			} else {
				rbody = PublicUtils.getReturn("绑定失败", "0");
			}
		}
		return rbody;
	}
	
	/**
	 * 修改用户信息
	 * @param customer
	 * @return
	 */
	@Transactional // 添加事务.
	public JSONObject upcustomerInfo(customer customer) {
		JSONObject rbody = null;
		int body = customerinfo.upcustomerInfo(customer);
		if (body > 0) {
			rbody = PublicUtils.getReturn("修改成功", "1");
		} else {
			rbody = PublicUtils.getReturn("修改失败", "0");
		}
		return rbody;
	}
	
	/**
	 * 修改密码
	 * @param newpassword
	 * @param customerName
	 * @param oldpassword
	 * @return
	 */
	@Transactional // 添加事务.
	public JSONObject upcustomerpassword(String newpassword,String customerName,String oldpassword) {
		JSONObject rbody = null;
		int body = customerinfo.upcustomerpassword(newpassword, customerName, oldpassword);
		if (body > 0) {
			rbody = PublicUtils.getReturn("修改成功", "1");
		} else {
			rbody = PublicUtils.getReturn("修改失败", "0");
		}
		return rbody;
	}

}
