package com.canteenordering.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.alibaba.fastjson.JSONObject;
import com.canteenordering.dao.noticeMapper;
import com.canteenordering.javabean.notice;
import com.canteenordering.utils.PublicUtils;

@Service
public class noticeService {

	@Autowired
	private noticeMapper noticeinfo;

	@Transactional // 添加事务.
	public JSONObject insertnoticeInfo(notice notice) {
		String code = PublicUtils.getRandomString(20);
		notice.setNoticeCode(code);
		JSONObject rbody = null;
		int index = noticeinfo.insertnoticeInfo(notice);
		if (index > 0) {
			rbody = PublicUtils.getReturn("新增成功", "1");
		} else {
			rbody = PublicUtils.getReturn("新增失败", "0");
		}
		return rbody;
	}

	@Transactional // 添加事务.
	public List<notice> getnoticeListInfo() {
		JSONObject rbody = null;
		List<notice> list = noticeinfo.getnoticeListInfo();
		return list;
	}
	@Transactional // 添加事务.
	public notice getnoticeInfo(String noticeCode) {
		JSONObject rbody = null;
		notice info = noticeinfo.getnoticeInfo(noticeCode);
		return info;
	}

	@Transactional // 添加事务.
	public JSONObject deletenotice(String noticeCode) {
		JSONObject rbody = null;
		int index = noticeinfo.deletenotice(noticeCode);
		if (index > 0) {
			rbody = PublicUtils.getReturn("删除成功", "1");
		} else {
			rbody = PublicUtils.getReturn("删除失败", "0");
		}
		return rbody;
	}

}
