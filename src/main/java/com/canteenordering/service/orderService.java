package com.canteenordering.service;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.alibaba.fastjson.JSONObject;
import com.canteenordering.dao.businessMapper;
import com.canteenordering.dao.orderMapper;
import com.canteenordering.javabean.business;
import com.canteenordering.javabean.order;
import com.canteenordering.javabean.orderCommodity;
import com.canteenordering.javabean.orderVO1;
import com.canteenordering.utils.PublicUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

@Service
public class orderService {

	@Autowired
	private orderMapper orderinfo;

	/**
	 * 订单新增
	 * 
	 * @param order
	 * @return
	 */
	@Transactional // 添加事务.
	public JSONObject insertorderInfo(order order) {

		System.out.println(order.toString());

		JSONObject rbody = null;
		String code=System.currentTimeMillis()+"";
		order.setOrderCode(code);
		System.out.println("code: " + code);
		Calendar now = Calendar.getInstance();
		System.out.println("年: " + now.get(Calendar.YEAR));
		System.out.println("月: " + (now.get(Calendar.MONTH) + 1) + "");
		System.out.println("日: " + now.get(Calendar.DAY_OF_MONTH));
		System.out.println("时: " + now.get(Calendar.HOUR_OF_DAY));
		System.out.println("分: " + now.get(Calendar.MINUTE));
		System.out.println("秒: " + now.get(Calendar.SECOND));
		System.out.println("当前时间毫秒数：" + now.getTimeInMillis());
		System.out.println(now.getTime());

		int index = orderinfo.insertorderInfo(order, (now.get(Calendar.MONTH) + 1), now.get(Calendar.DAY_OF_MONTH),
				now.get(Calendar.YEAR) + "");
		if (index > 0) {
			List<orderCommodity> OrderCommodityList = order.getOrderCommodity();
			for (orderCommodity orderCommodity : OrderCommodityList) {
				orderCommodity.setOrderCode(code);
				orderinfo.insertordercommodityInfo(orderCommodity);
			}
			JSONObject rjson = new JSONObject();
			rjson.put("flag", 1);
			rjson.put("msg", "新增成功");
			rjson.put("orderCode", code);
			rbody = rjson;
		} else {
			rbody = PublicUtils.getReturn("新增失败", "0");
		}
		return rbody;
	}

	/**
	 * 订单列表
	 * 
	 * @param page
	 * @param pageNum
	 * @param state
	 * @param businessCode
	 * @param bphone
	 * @param bname
	 * @param customerCode
	 * @param cname
	 * @param cphone
	 * @return
	 */
	@Transactional // 添加事务.
	public PageInfo<order> getorderListInfo(int page, int pageNum, String state, String businessCode, String bphone,
			String bname, String customerCode, String cname, String cphone,String stateTime,String endTime) {
		PageHelper.startPage(page, pageNum);
		List<order> body = orderinfo.getorderListInfo(state, businessCode, bphone, bname, customerCode, cname, cphone, stateTime, endTime);
		for (order order : body) {
			List<orderCommodity> CommodityInfo = orderinfo.getorderCommodityInfo(order.getOrderCode());
			order.setOrderCommodity(CommodityInfo);
		}
		PageInfo<order> info = new PageInfo<>(body);
		return info;
	}

	/**
	 * 下载表格
	 * 
	 * @param state
	 * @param businessCode
	 * @param bphone
	 * @param bname
	 * @param customerCode
	 * @param cname
	 * @param cphone
	 * @return
	 */
	@Transactional // 添加事务.
	public List<order> getorderListInfo(String state, String businessCode, String bphone, String bname,
			String customerCode, String cname, String cphone,String stateTime,String endTime) {
		List<order> body = orderinfo.getorderListInfo(state, businessCode, bphone, bname, customerCode, cname, cphone, stateTime, endTime);
		for (order order : body) {
			List<orderCommodity> CommodityInfo = orderinfo.getorderCommodityInfo(order.getOrderCode());
			order.setOrderCommodity(CommodityInfo);
		}
		return body;
	}

	/**
	 * 订单详情
	 * 
	 * @param orderCode
	 * @return
	 */
	@Transactional // 添加事务.
	public order getorderInfo(String orderCode) {
		JSONObject rbody = null;
		order body = orderinfo.getorderInfo(orderCode);
		List<orderCommodity> CommodityInfo = orderinfo.getorderCommodityInfo(orderCode);
		body.setOrderCommodity(CommodityInfo);
		return body;
	}

	/**
	 * 修改订单状态
	 * 
	 * @param state
	 * @param orderCode
	 * @return
	 */
	@Transactional // 添加事务.
	public JSONObject updateorderstate(int state, String orderCode) {
		JSONObject rbody = null;
		int body = orderinfo.updateorderstate(state, orderCode);
		if (body > 0) {

			rbody = PublicUtils.getReturn("修改成功", "1");
		} else {
			rbody = PublicUtils.getReturn("修改失败", "0");
		}
		return rbody;
	}

	/**
	 * 催单
	 * 
	 * @param orderCode
	 * @return
	 */
	@Transactional // 添加事务.
	public JSONObject updateorderreminderNum(String orderCode) {
		JSONObject rbody = null;
		int body = orderinfo.updateorderreminderNum(orderCode);
		if (body > 0) {
			rbody = PublicUtils.getReturn("催单成功", "1");
		} else {
			rbody = PublicUtils.getReturn("催单失败", "0");
		}
		return rbody;
	}

	/**
	 * 统计图
	 * 
	 * @param year
	 * @param month
	 * @param type
	 * @return
	 */
	@Transactional // 添加事务.
	public List<orderVO1> getordertongji(String year, String month, int type,String businessCode) {
		List<orderVO1> body = orderinfo.getordertongji(year, month, type,businessCode);
		return body;
	}

}
